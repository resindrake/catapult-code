#include <IRremote.h>
#include <Servo.h>

const int tickrate = 50; // Tick duration (ms)

const int actuator_pin = 7; // analog
const int ir_pin = 12; // digital
const int servo_pin = 11; // digital

bool isActuating = false;
int actuatorTimeRemaining = 10;
const int actuatorDuration = 5;
int angle_increment = 0;

IRrecv irrecv(ir_pin);
decode_results results;
char buffer0[8];
char buffer1[10];
int out;

int angle = 0; // servo target inclination
String angle_str = ""; // stores 2 numbers, then is assigned to angle and is cleared
int angle_str_count = 0; // how many numbers have been entered

Servo myservo; // see Servo.h

// Buttons
const int button_ch_minus = 0x00FFA25D;
const int button_ch_plus = 0x00FFE21D;
const int button_ch = 0x00FF629D;
const int button_prev = 0x00FF22DD;
const int button_next = 0x00FF02FD;
const int button_playpause = 0x00FFC23D;
const int button_vol_minus = 0x00FFE01F;
const int button_vol_plus = 0x00FFA857;
const int button_eq = 0x00FF906F;
const int button_0 = 0x00FF6897;
const int button_100_plus = 0x00FF9867;
const int button_200_plus = 0x00FFB04F;
const int button_1 = 0x00FF30CF;
const int button_2 = 0x00FF18E7;
const int button_3 = 0x00FF7A85;
const int button_4 = 0x00FF10EF;
const int button_5 = 0x00FF38C7;
const int button_6 = 0x00FF5AA5;
const int button_7 = 0x00FF42BD;
const int button_8 = 0x00FF4AB5;
const int button_9 = 0x00FF52AD;
const int repeat = 0xFFFFFFFF;

void setup() {
	IR_setup();
	servo_setup();
  actuator_setup();
}

void loop() {
	switch (get_button()) {
		case button_ch_minus: set_angle(0); break;
		case button_ch: set_angle(45); break;
    case button_ch_plus: set_angle(90); break;
    
    case button_prev: increment_angle(-10); break;
    case button_next: increment_angle(10); break;
    case button_vol_minus: increment_angle(-1); break;
    case button_vol_plus: increment_angle(1); break;
    
		case button_eq: clear_number(); break;
    
		case button_playpause: actuate(); break;
    
    case button_100_plus: set_servo_auto(3); break;
    case button_200_plus: set_servo_auto(10); break;
    
		case button_0: append_number(0); break;
		case button_1: append_number(1); break;
		case button_2: append_number(2); break;
		case button_3: append_number(3); break;
		case button_4: append_number(4); break;
		case button_5: append_number(5); break;
		case button_6: append_number(6); break;
		case button_7: append_number(7); break;
		case button_8: append_number(8); break;
		case button_9: append_number(9); break;
	}

	actuator_loop();
	servo_loop();
	delay(tickrate);
}

void actuator_setup(){
  
}

void actuate() {
  actuatorTimeRemaining = actuatorDuration;
  isActuating = true;
  Serial.println("On");
}

void actuator_loop() {
  analogWrite(actuator_pin, isActuating ? 255 : 0);
  if (actuatorTimeRemaining == 0) {
    isActuating = false;
    Serial.println("Off");
    actuatorTimeRemaining--;
  } else {
    actuatorTimeRemaining--;
  }
}

void IR_setup() {
  Serial.begin(9600);
  irrecv.enableIRIn();
}

int get_button() {
  out = 0x00000000;
  if (irrecv.decode(&results)) {
    out = results.value;
    irrecv.resume();
  }
  return out;
}

void servo_setup() {
  myservo.attach(servo_pin);
}

void servo_loop() {
  if (not increment_angle(angle_increment)) {
    angle_increment = -angle_increment;
  }
  myservo.write(angle);
}

bool set_angle(int agl) {
  angle = min(max(agl, 0), 90);
  return angle == agl;
}

bool increment_angle(int agl) {
  return set_angle(angle + agl);
}

void clear_number() {
  angle_str = "";
  angle_str_count = 0;
}

void append_number(int number) {
  angle_str = angle_str + number;
  angle_str_count++;

  if (angle_str_count == 2) {
    set_angle(angle_str.toInt());
    clear_number();
    Serial.println(angle);
  }
}


void set_servo_auto(int n){
  if (abs(angle_increment) == n) {
    angle_increment = 0;
  } else {
    angle_increment = n;
  }
}


