# catapult-code

## Specs of code
* Able to receive input from IR remote
	* Read numbers pressed followed by EQ to set servo (e.g. 4, 5, EQ sets angle to 45 degrees)
	* Retract actuator for 1 second when playpause button is pressed to fire catapult
* Able to retract/extend solenoid (To release the mechanism and thus fire the catapult)
* Able to set angle of servo to value specified by remote (To set angle of catapult)
